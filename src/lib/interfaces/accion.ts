/**
 *  Para definir el ancho de la columna de acciones, crear una columna con el nombre Acciones
 */
export interface Accion {
	color?: Function;
	condicion?: Function;
	icon: Function;
	handler?: Function;
	title?: Function;
}
