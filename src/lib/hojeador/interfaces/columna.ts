import { Accion } from '@componentes/interfaces';

export interface ColumnaHojeador {
	def: string;
	nombre: string;
	ancho?: number;
	tipo: 'numero' | 'texto' | 'fecha' | 'monto' | 'comprobante' | 'imagen' | 'boton';
	modifica?: boolean;
	clase?: string;
	modificaDatos?: {
		remoto: boolean,
		que?: string;
		datos?: Function;

		/**
		 * Procesa el dato antes de enviar al InputBS
		 * (procesa: Function) procesa(element, columna)
		 */
		procesa?: Function;

		/**
		 * Actualiza el dato en la BD
		 * (actualiza: Function) actualiza(element[def], res)
		 */
		actualiza?: Function;
	};
	accion?: Accion[];
}
