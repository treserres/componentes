import { Accion } from '@componentes/interfaces';
import { ColumnaHojeador } from './columna';

export interface HojeadorPrefs {
    /** Definición de columnas de tabla primaria */
    columnas?: ColumnaHojeador[];

    /** Resultados por página */
    cuantos?: number;

    /** Si muestra checkbox y emite eventos */
    selecciona?: boolean;

    /** Nombre del campo que referencia al id. Ej: "ctid" */
    nombreCampoId?: string;

    /** Nombre del campo que referencia al nombre. Ej: "ctnombre" */
    nombreCampoDenominacion?: string;

    /** Retorna el objeto completo en el selecciona */
    retornarObjetoCompleto?: boolean;

    /** "Elementos seleccionadas": 1 */
    nombreElementosSeleccionados?: string;

    /** Si hace efecto al hover y busca detalle */
    detalla?: boolean;

    /** Si se incluye un buscador y agregar campo 'texto' a las requests */
    busca?: string;

    /** Array de acciones (tabla primaria) */
    acciones?: Accion[];

    /** Si se detalla, esta es la estructura de la subtabla */
    columnasSubtabla?: ColumnaHojeador[];

    /** Array de acciones de la subtabla */
    accionesSubtabla?: Accion[];

    /** Ruta query. Ej: admin/hojear/usuarios */
    url?: string;

    /** Si no es null (por defecto), muestra un toggle-slide */
    toggle?: {
        checked: Function;
        onChange: Function;
    };

    /** Si está contenido en un sidenav, por scrolling */
	sidenav?: boolean;

    /** Si pagina al mover la rueda */
    scrollable?: boolean;

    /** Si se utiliza un disparador personalizado para expandir el detalle */
    customTrigger?: boolean;

    /** Si realiza las peticiones por POST, caso contrario, GET */
    post?: boolean;

    /** Si muestra prefijo con "$" */
    mostrarPeso?: boolean;

    /** Función que brinda el componente padre para modificar los datos al refrescar */
    modificarDatos?: Function;

    /** Función que brinda el componente padre para obtener los parámetros de la query */
    getDatos?: Function;

    /** Función que brinda el componente padre para obtener los parámetros de la query */
    buscarDetalle?: Function;


}
