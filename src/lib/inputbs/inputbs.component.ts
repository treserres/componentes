import { Component, Inject, AfterViewInit, ViewEncapsulation, ViewChild, ElementRef, Input } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material';
import { HttpService } from '../http.service';
import { AutocompleteComponent } from '@componentes/autocomplete';
import { FechaComponent } from '@componentes/fecha';
import { FormGroup } from '@angular/forms';

@Component({
	selector: 'tr-inputbs',
	templateUrl: 'inputbs.component.html',
	styleUrls: ['inputbs.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class InputBSComponent implements AfterViewInit {
	constructor(
		@Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
		private elementRef: MatBottomSheetRef<InputBSComponent>,
		private httpService: HttpService
	) { }

	resultados: any[] = [];
	formDefault = new FormGroup({ });

	@Input() form: FormGroup;
	@ViewChild('input') inputElement: ElementRef;
	@ViewChild(AutocompleteComponent) autocompleteComponent: AutocompleteComponent;
	@ViewChild(FechaComponent) fechaComponent: FechaComponent;

	ngOnInit() {
		if (!this.form) {
			this.form = this.formDefault;
		}
	}
	ngAfterViewInit() {
		
		if (this.inputElement) {
			this.elementRef.afterOpened().toPromise().then(() => {
				this.inputElement.nativeElement.focus();
			});
		} else if (this.fechaComponent){
			// console.log(this.fechaComponent);
			this.fechaComponent.date = this.data.fecha;
		} else {
			if (typeof this.data.autocomplete === 'string') {
				this.autocompleteComponent.busca = this.data.autocomplete;
			} else {
				this.autocompleteComponent.resultados = this.data.autocomplete;
			}
			this.elementRef.afterOpened().toPromise().then(() => {
				this.autocompleteComponent.focus();
			});
		}
	}

	cerrar(value?): void {
		if (this.fechaComponent){
			this.data['value'] = this.fechaComponent.modelo;
		} else if (value) {
			for (const key in value) {
				if (value.hasOwnProperty(key)) {
					this.data[key] = value[key];
				}
			}
		}
		this.elementRef.dismiss(this.data);
	}

	busAutocomplete(texto) {
		if (typeof this.data.autocomplete === 'string') {
			if (texto.length === 0) {
				this.resultados = [];
			} else {
				this.httpService.autocomplete(this.data.autocomplete, texto).then((data) => {
					this.resultados = [];
					if (data.length > 3) {
						for (let i = 0; i < 3; i++) {
							this.resultados.push(data[i]);
						}
					} else {
						this.resultados = data;
					}
				});
			}
		} else {
			if (texto.length === 0) {
				this.resultados = [];
			} else if (typeof texto === 'string') { // Porque del autocomplete vuelve un objeto al ser seleccionado
				const re = new RegExp(texto.toLowerCase());
				this.resultados = this.data.autocomplete.filter((val) => {
					const label = val.label.toLowerCase();
					return re.test(label);
				});
			}
		}
	}
	

	onFocus(event) {
		if (event.target.select) {
			event.target.select();
		}
	}
}
